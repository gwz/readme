Welcome! I am well-versed in methods such as [generalized linear models](https://gitlab.com/gwz/stats), [dimensionality reduction, ensemble learning](https://gitlab.com/gwz/ds/reviews-prediction), and working knowledge of others like recurrent neural networks. I am familiar with statistical methods like [kernel smoothing](https://gitlab.com/gwz/ml/bandwidth-kernel-density-classification), [time series analysis](https://gitlab.com/gwz/stats/cc) and point process analysis.

Check out **[our analysis of Boston housing prices](https://gitlab.com/gwz/dv/housing)**. Using generalized linear models, we calculated premiums for features (e.g., central air, +16% rent increase) and improved predictions for housing rental prices. 

Check out my solution for **[prediction for the number of stars](https://gitlab.com/gwz/ds/reviews-prediction)** associated with an Amazon review. I achieved excellent performance by using valence scores calculated from bigrams in the text, fed to a random forest regressor.

For some thoughts on reinforcement learning, refer to Section 5 of [my recent arXiv paper](https://gitlab.com/gwz/arxiv/predict-future). 

Thanks for stopping by!

[Back to my portfolio](https://gitlab.com/gwz)
